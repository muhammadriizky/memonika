<footer role="contentinfo" style="padding-top: 10px">
    <div class="container">

        <div class="row copyright">
            <div class="col-md-12 text-center">
                <p>
                    <small class="block">&copy; 2019 Akadin.id - All Rights Reserved.</small> 
                </p>
            </div>
        </div>

    </div>
</footer>

<div class="mobile-bottom-bar">
    <a href="#fh5co-header" class="footer-link">
        <i class="fas fa-home"></i> 
        <p class='footer-text'>Home</p>
    </a>
    <a href="#fh5co-couple" class="footer-link">
        <i class="fas fa-heart"></i>
        <p class='footer-text'>Mempelai</p>
    </a>
    <a href="#fh5co-event" class="footer-link">
        <i class="far fa-calendar-alt"></i>
        <p class='footer-text'>Acara</p>
    </a>
    <a href="#fh5co-gallery" class="footer-link">
        <i class="far fa-images"></i>
        <p class='footer-text'>Gallery</p>
    </a>
</div>