<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Memonika - Undangan Nikah Digital</title>
        @extends('layouts.head')
    </head>
    <body>
    @extends('layouts.header')

    
        <section class="ftco-section services-section bg-light ftco-no-pb">
            <div class="container">
                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-8 text-center heading-section ftco-animate">
                <h1 class="mb-3 h3"; style="text-align:center;">KEBIJAKAN & PRIVASI</h1>
                Kami menjaga privasi pelanggan kami dengan serius dan kami hanya akan mengumpulkan, 
merekam, menahan, menyimpan, mengungkapkan, dan menggunakan informasi 
pribadi Anda seperti yang diuraikan di bawah.
                </div>
                </div>
                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-8 text-justify heading-section ftco-animate">
                <h1 class="mb-3 h3">Pengumpulan Informasi Pribadi</h1>
                Kami hanya akan dapat mengumpulkan informasi pribadi Anda jika Anda secara sukarela 
menyerahkan informasi kepada kami. Jika Anda memilih untuk tidak mengirimkan informasi 
pribadi Anda kepada kami atau kemudian menarik persetujuan menggunakan informasi 
pribadi Anda, maka hal itu dapat menyebabkan kami tidak dapat menyediakan layanan 
kami kepada Anda. Anda dapat mengakses dan memperbarui informasi pribadi yang 
Anda berikan kepada kami setiap saat seperti yang dijelaskan di bawah.
Jika Anda memberikan informasi pribadi dari pihak ketiga manapun kepada kami, maka 
kami menganggap bahwa Anda telah memperoleh izin yang diperlukan dari pihak ketiga 
terkait untuk berbagi dan mentransfer informasi pribadinya kepada kami.
Jika Anda mendaftar menggunakan akun media sosial Anda atau menghubungkan akun 
memonika Anda ke akun media sosial Anda, kami dapat mengakses informasi tentang 
Anda yang terdapat dalam media sosial Anda sesuai dengan kebijakan penyedia sosial 
media bersangkutan, dan kami akan mengelola data pribadi Anda yang telah kami
Pengumpulan Informasi Pribadi
Kami hanya akan dapat mengumpulkan informasi pribadi Anda jika Anda secara sukarela 
menyerahkan informasi kepada kami. Jika Anda memilih untuk tidak mengirimkan informasi 
pribadi Anda kepada kami atau kemudian menarik persetujuan menggunakan informasi 
pribadi Anda, maka hal itu dapat menyebabkan kami tidak dapat menyediakan layanan 
kami kepada Anda. Anda dapat mengakses dan memperbarui informasi pribadi yang 
Anda berikan kepada kami setiap saat seperti yang dijelaskan di bawah.
Jika Anda memberikan informasi pribadi dari pihak ketiga manapun kepada kami, maka 
kami menganggap bahwa Anda telah memperoleh izin yang diperlukan dari pihak ketiga 
terkait untuk berbagi dan mentransfer informasi pribadinya kepada kami.
Jika Anda mendaftar menggunakan akun media sosial Anda atau menghubungkan akun 
memonika Anda ke akun media sosial Anda, kami dapat mengakses informasi tentang 
Anda yang terdapat dalam media sosial Anda sesuai dengan kebijakan penyedia sosial 
media bersangkutan, dan kami akan mengelola data pribadi Anda yang telah kami
kumpulkan sesuai dengan kebijakan privasi memonika
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-8 text-justify heading-section ftco-animate">
                <h1 class="mb-3 h3">Penggunaan dan Pengungkapan Informasi Pribadi</h1>
                pihak ketiga (termasuk perusahaan terkait, penyedia jasa layanan pihak ketiga, dan 
penjual pihak ketiga), untuk beberapa atau semua tujuan berikut:
<ol>

<li> Untuk memfasilitasi penggunaan Layanan (sebagaimana didefinisikan dalam 
Syarat & Ketentuan) dan / atau akses ke Platform</li>
<li> Selanjutnya, kami akan menggunakan informasi yang Anda berikan untuk memverifikasi 
dan melakukan transaksi keuangan dalam kaitannya dengan pembayaran yang Anda 
buat secara online, mengaudit pengunduhan data dari Platform, memperbarui layout 
dan atau konten dari halaman Platform dan menyesuaikannya untuk pengguna, 
mengidentifikasi pengunjung pada Platform, melakukan penelitian tentang demografi 
dan perilaku pengguna kami; menyediakan informasi yang kami pikir mungkin 
berguna bagi Anda atau yang telah Anda minta dari kami, termasuk informasi 
tentang produk dan layanan kami atau penjual pihak ketiga, dimana jika Anda 
telah tidak keberatan dihubungi untuk tujuan ini</li>
</ol>
Memonika dapat menggunakan informasi pribadi Anda dengan pihak ketiga dan afiliasi kami 
untuk tujuan tersebut di atas, khususnya, menyelesaikan transaksi dengan anda dan dalam 
rangka pemasaran dan pemenuhan persyaratan hukum atau peraturan dan permintaan yang 
dianggap perlu oleh memonika. Dalam hal ini, kami berusaha untuk memastikan bahwa pihak 
ketiga dan afiliasi kami menjaga informasi pribadi Anda aman dari akses yang tidak sah, 
pengumpulan, penggunaan, pengungkapan, atau risiko sejenis dan menyimpan informasi 
pribadi Anda selama informasi pribadi Anda dibutuhkan untuk tujuan yang disebutkan di atas.
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-8 text-justify heading-section ftco-animate">
                <h1 class="mb-3 h3">Keamanan Informasi Pribadi Anda</h1>
                Memonika memastikan bahwa seluruh informasi yang dikumpulkan tersimpan dengan aman. 
Kami menjaga informasi pribadi Anda dengan cara:
<ol>

<li> Membatasi akses ke informasi pribadi</li>
<li> Mengikuti kemajuan teknologi pengamanan untuk mencegah akses komputer tidak sah</li>
<li> Dengan aman menghilangkan informasi pribadi Anda ketika tidak lagi digunakan 
untuk keperluan hukum atau bisnis.</li>
</ol>
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-8 text-justify heading-section ftco-animate">
                <h1 class="mb-3 h3">Perubahan pada Kebijakan Privasi</h1>
                Memonika dapat secara berkala meninjau kecukupan Kebijakan Privasi ini. Kami berhak 
untuk memodifikasi dan mengubah kebijakan privasi setiap saat. Setiap perubahan kebijakan 
ini akan dipublikasikan pada Platform
                </div>
                </div>

                <div class="row justify-content-center mb-5 pb-3">
                <div class="col-md-8 text-justify heading-section ftco-animate">
                <h1 class="mb-3 h3">Hak memonika</h1>
                ANDA MEMAHAMI DAN MENYETUJUI BAHWA MEMONIKA MEMILIKI HAK UNTUK MENGUNGKAPKAN 
INFORMASI PRIBADI ANDA PADA SETIAP HUKUM, PERATURAN, PEMERINTAHAN, PAJAK, PENEGAKAN 
HUKUM ATAU PEMERINTAH ATAU PEMILIK HAK TERKAIT, JIKA BRIDENESIA MEMILIKI ALASAN WAJAR 
YANG DAPAT DIPERCAYA BAHWA PENGUNGKAPAN INFORMASI PRIBADI ANDA DIPERLUKAN UNTUK 
KEWAJIBAN APAPUN, SEBAGAI PERSYARATAN ATAU PENGATURAN, BAIK SUKARELA ATAU WAJIB, 
SEBAGAI AKIBAT DARI PESANAN, PEMERIKSAAN DAN / ATAU PERMINTAAN PIHAK TERKAIT. SEJAUH 
DIIZINKAN OLEH HUKUM YANG BERLAKU, DALAM HAL INI ANDA SETUJU UNTUK TIDAK MELAKUKAN 
TUNTUTAN APAPUN TERHADAP BRIDENESIA UNTUK PENGUNGKAPAN INFORMASI PRIBADI ANDA.
                </div>
                </div>
        </section>

      
    @extends('layouts.footer')
    @extends('layouts.script')
    </body>
</html>
