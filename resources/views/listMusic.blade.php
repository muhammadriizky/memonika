<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>ADMIN MEMONIKA</title>
    @extends('parts.style')
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
@extends('parts.sidebar')

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper" style="margin-top: 0 !important">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2 mt-5">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">List Musik</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a href="" />Home</a></li>
                        <li class="breadcrumb-item active">List Musik</li>
                    </ol>
                </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <a href="#" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">Tambah Musik</a>
                    </div>
                    <div class="card-body table-responsive content-desktop">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th></th>
                                <th style="width: 40%">Filename</th>
                                <th>URL</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                 @foreach($music as $i => $m)
                                <audio src="<?= $m->url ?>" id="audio<?= $i ?>"></audio>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td>
                                        <a href="#" onclick="play<?= $i ?>()" id="play<?= $i ?>" class="fas fa-play-circle" ></a>
                                        <a href="#" onclick="stop<?= $i ?>()" id="stop<?= $i ?>" class="fas fa-stop-circle" style="display:none"> </a>
                                    </td>
                                    <td><?= $m->filename ?></td>
                                    <td>
                                        <?= $m->url ?><br>
                                        <a href="#" class="btn btn-success btn-sm copyURL" data-clipboard-text="<?= $m->url ?>">copy URL</a>
                                    </td>
                                    <td>
                                        <a href="('dasbor/delMusic?id='.$m->id)" class="btn btn-danger btn-sm" onclick="return del_confirm()">Hapus</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>

                    <div class="card-body table-responsive content-mobile">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th></th>
                                <th>Filename</th>
                            </thead>
                            <tbody>
                                @foreach($music as $i => $m)
                                <audio src="<?= $m->url ?>" id="audio<?= $i ?>"></audio>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td>
                                        <a href="#" onclick="play<?= $i ?>()" id="play<?= $i ?>" class="fas fa-play-circle" ></a>
                                        <a href="#" onclick="stop<?= $i ?>()" id="stop<?= $i ?>" class="fas fa-stop-circle" style="display:none"> </a>
                                    </td>
                                    <td style="width:60%">
                                        <?= $m->filename ?><br>
                                        <a href="#" class="btn btn-success btn-sm copyURL" data-clipboard-text="<?= $m->url ?>">copy URL</a>
                                        <a href="('dasbor/delMusic?id='.$m->id)" class="btn btn-danger btn-sm" onclick="return del_confirm()">Hapus</a>
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
        <!-- Control sidebar content goes here -->
    </aside>
    <!-- /.control-sidebar -->
    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Musik</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
         
            
            <div class="form-group row">
                <label for="" class="col-md-3">Upload File Musik</label>
                <div class="col-md-9">
                    <input type="file" class="form-control" name="musik">
                </div>
            </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
            </div>
        </div>
    </div>
    <!-- Main Footer -->
    <footer class="main-footer">
        <strong>Copyright &copy; <?= date('Y') ?> akadin.ID</strong>
        All rights reserved.
        <div class="float-right d-none d-sm-inline-block">
        Powered by <b>Gidicode Project</b>
        </div>
    </footer>
</div>
<!-- ./wrapper -->

@extends('parts.script')

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="{{ asset('assets/admin/plugins/jquery-mousewheel/jquery.mousewheel.js') }}" defer></script>
<script src="{{ asset('assets/admin/plugins/raphael/raphael.min.js') }}" defer></script>
<script src="{{ asset('assets/admin/plugins/jquery-mapael/jquery.mapael.min.js') }}" defer></script>
<script src="{{ asset('assets/admin/plugins/jquery-mapael/maps/usa_states.min.js') }}" defer></script>
<!-- ChartJS -->
<script src="{{ asset('assets/admin/plugins/chart.js/Chart.min.js') }}" defer></script>

<!-- PAGE SCRIPTS -->
<script src="{{ asset('assets/admin/js/pages/dashboard2.js') }}" defer></script>

<script src="{{ asset('assets/admin/plugins/clipboard/dist/clipboard.js') }}" defer></script>

@foreach($music as $i => $m)
<script>
    function play<?= $i ?>() {
        var audio<?= $i ?> = document.getElementById('audio<?= $i ?>');
        audio<?= $i ?>.play();
        $('#play<?= $i ?>').removeClass('d-flex');
        $('#play<?= $i ?>').addClass('d-none');
        $('#stop<?= $i ?>').addClass('d-flex');
        /*
        if (audio<?= $i ?>.paused) {
            audio<?= $i ?>.play();
            $('#play<?= $i ?>').removeClass('fa-play-circle')
            $('#play<?= $i ?>').addClass('fa-stop-circle')
        }else{
            audio<?= $i ?>.pause();
            audio<?= $i ?>.currentTime = 0
            $('#play<?= $i ?>').addClass('fa-play-circle')
            $('#play<?= $i ?>').removeClass('fa-stop-circle')
        } */
    }

    function stop<?= $i ?>() {
        var audio<?= $i ?> = document.getElementById('audio<?= $i ?>');
        audio<?= $i ?>.pause();
        $('#stop<?= $i ?>').removeClass('d-flex');
        $('#stop<?= $i ?>').addClass('d-none');
        $('#play<?= $i ?>').addClass('d-flex');
    }
</script>
@endforeach

<script>
    new ClipboardJS('.copyURL');
    clipboard.on('success', function(e) {
        console.info('Action:', e.action);
        console.info('Text:', e.text);
        console.info('Trigger:', e.trigger);

        e.clearSelection();
    });

    clipboard.on('error', function(e) {
        console.error('Action:', e.action);
        console.error('Trigger:', e.trigger);
    });
</script>
</body>
</html>
