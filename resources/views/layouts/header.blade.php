<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">Memonika</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>

        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item "><a href="/" class="nav-link">Home</a></li>
                <li class="nav-item "><a href="index.html" class="nav-link">Contoh Undangan</a></li>
                <li class="nav-item dropdown">
                <a href="about.html" class="nav-link dropdown-toggle" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Panduan Pengguna</a>
  <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
    <a class="dropdown-item" href="{{ url('/memonika') }}">Memonika</a>
    <a class="dropdown-item" href="{{ url('/faq') }}">FAQ</a>
    <a class="dropdown-item" href="{{ url('/panduanpengguna') }}">Panduan</a>
    <a class="dropdown-item" href="{{ url('/privacypolicy') }}">Privacy Policy</a>
  </div>
  </li>
                <li class="nav-item"><a href="about.html" class="nav-link">Inspirasi</a></li>
                <li class="nav-item cta"><a href="create" class="nav-link"><span>Buat Undangan</span></a></li>
            </ul>
        </div>
    </div>
</nav>
<!-- END nav -->