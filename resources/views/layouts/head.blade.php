<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link href="https://fonts.googleapis.com/css?family=Work+Sans:300,400,500,600,700" rel="stylesheet">

<link href="{{ asset('assets/front/css/open-iconic-bootstrap.min.css') }}" rel="stylesheet">
<link href="{{ asset('assets/front/css/open-iconic-bootstrap.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/front/css/animate.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('assets/front/css/owl.carousel.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/front/css/owl.theme.default.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/front/css/magnific-popup.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/front/css/carousel.min.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('assets/front/css/aos.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('assets/front/css/ionicons.min.css') }}" rel="stylesheet" type="text/css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.11.2/css/all.css">

<link href="{{ asset('assets/front/css/bootstrap-datepicker.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/front/css/jquery.timepicker.css') }}" rel="stylesheet" type="text/css">

<link href="{{ asset('assets/front/css/flaticon.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/front/css/icomoon.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('assets/front/css/style.css') }}" rel="stylesheet" type="text/css">
