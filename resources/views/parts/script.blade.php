<!-- jQuery -->
<script src="{{ asset('assets/admin/plugins/jquery/jquery.min.js') }}" defer></script>
<!-- Bootstrap 4 -->
<script src="{{ asset('assets/admin/plugins/bootstrap/js/bootstrap.bundle.min.js') }}" defer></script>
<!-- AdminLTE App -->
<script src="{{ asset('assets/admin/js/adminlte.min.js') }}" defer></script>

<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>


<script>
    $(document).ready(function() {
        $('.data-table').DataTable();
    } );

    $("#alert-dismiss").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert-dismiss").slideUp(500);
    });

    function del_confirm() {
        var r = confirm("Are you sure?");
        if (!r) {
            return false;
        } else {
            return true;
        }
    }
</script>