<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'HomepageController@index')->name('home');
Route::get('/memonika', 'HomepageController@memonika')->name('memonika');
Route::get('/faq', 'HomepageController@faq')->name('faq');
Route::get('/panduanpengguna', 'HomepageController@panduanpengguna')->name('panduanpengguna');
Route::get('/privacypolicy', 'HomepageController@privacypolicy')->name('privacypolicy');
Route::get('/create', 'HomepageController@create')->name('create');
Route::get('/create/addon', 'HomepageController@create_addon')->name('create_addon');
Route::get('/create_addon', 'HomepageController@createAddon')->name('createAddon');
Route::get('/create_data', 'HomepageController@createData')->name('createData');
Route::get('/create_pay', 'HomepageController@createPay')->name('createPay');
Route::get('/dashboard', 'DashboardController@index')->name('dashboard');
Route::get('/invitation/add', 'DashboardController@addInv')->name('addInv');
Route::get('/invitation/list', 'DashboardController@listInv')->name('listInv');
Route::get('/invitation/template', 'DashboardController@listTemplate')->name('listTemplate');
Route::get('/invitation/music', 'DashboardController@listMusic')->name('listMusic');
Route::get('/invitation/{id}/edit', 'DashboardController@EditInv')->name('EditInv');
Route::get('/example/{filename}', 'InvitationController@Example')->name('Example');

