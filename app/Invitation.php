<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Sofa\Eloquence\Eloquence;

class Invitation extends Model
{
    protected $table = 'invitations';

    protected $guarded = [];
}
